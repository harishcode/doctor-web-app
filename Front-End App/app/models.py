from sqlalchemy import Column, Integer, String, ForeignKey
from database import Base
from sqlalchemy.orm import relationship, backref 


class Speciality(Base):
  __tablename__= 'tb_speciality'
  name=Column(String(100),primary_key=True)
 	
class Doctor(Base):
  __tablename__ = 'tb_doctor'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String(100), nullable = False)
  email = Column(String(100), nullable = False, unique=True)
  experience = Column(Integer, nullable = False)
  education = Column(String(100), nullable = False)
  clinics=relationship('DoctorsClinic',backref='doctors');
  speciality = relationship('DoctorsSpeciality',backref="specialities");
  
  def __init__(self, name, email, experience,education):
    self.name = name
    self.email = email
    self.experience = experience
    self.education = education

  def __repr__(self):
    return '<Doctor %r>' % (self.name)

class DoctorsSpeciality(Base):
  __tablename__='tb_doctorsspeciality'
  id = Column(Integer, primary_key=True, autoincrement=True)
  doctorIdFK=Column(Integer, ForeignKey(Doctor.id)); 
  specialityFK=Column(String(100),ForeignKey(Speciality.name));
  specialityInfo=relationship('Speciality',backref='speciality'); 


class Clinic(Base):
  __tablename__ = 'tb_clinic'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String(50), nullable = False)
  area = Column(String(120), nullable = False)
  address = Column(String(300), nullable = False)
  city_name = Column(String(120), nullable = False)
  contact_no = Column(String(120), nullable = False, unique =True)

  def __init__(self,name, area, address, city_name, contact_no):
    self.name = name
    self.area = area
    self.address = address
    self.city_name = city_name
    self.contact_no = contact_no

  def __repr__(self):
    return '<Clinic %r>' % (self.name)

class DoctorsClinic(Base):
  __tablename__='tb_doctorsclinic'
  id = Column(Integer, primary_key=True, autoincrement=True)
  doctorIdFK=Column(Integer, ForeignKey(Doctor.id),ForeignKey(Doctor.id,ondelete='CASCADE')); 
  clinicIdFk=Column(Integer,ForeignKey(Clinic.id),ForeignKey(Clinic.id,ondelete='CASCADE'));
  fees = Column(Integer, nullable = False)
  timings = Column(String(120), nullable = False)
  clinics=relationship('Clinic',backref='clinic') 
  def __init__(self,fees,timings): 
    self.fees = fees
    self.timings = timings

