from database import *
from models import *

init_db()

d1=Doctor('Ramji','r@gmail.com',12,'MBBS');
d2=Doctor('Ateeq','A@gmail.com',15,'MD');
d3=Doctor('Bharadwaj','B@gmail.com',10,'FRCS');
d4=Doctor('Izaz','I@gmail.com',12,'MBBS');

c1=Clinic('A-Clinic','JP Nagar','3rd phase','Bangalore',9885032939);
c2=Clinic('B-Clinic','Kormangala','3rd phase','Hyderabad',9885032938);
c3=Clinic('C-Clinic','JP Nagar','2nd phase','Hyderabad',9885032937);
c4=Clinic('D-Clinic','Benerghatta','3rd phase','Bangalore',9885032936);

s1=Speciality();
s1.name='Gynacologist';
s2=Speciality();
s2.name='Pediatrics';
s3=Speciality();
s3.name='Orthopedic';

ds1=DoctorsSpeciality();
ds1.speciality=s1;
d1.speciality.append(ds1);
ds2=DoctorsSpeciality();
ds2.speciality=s2;
d1.speciality.append(ds2);
ds3=DoctorsSpeciality();
ds3.speciality=s1;
d2.speciality.append(ds2);
ds4=DoctorsSpeciality();
ds4.speciality=s2;
d3.speciality.append(ds4);
d4.speciality.append(ds2);
dc1=DoctorsClinic(100,'1pm-3pm');
dc1.clinics=c1;
d1.clinics.append(dc1);
dc2=DoctorsClinic(200,'10am-12pm');
dc2.clinics=c2;
d1.clinics.append(dc2);
d2.clinics.append(dc2);
dc3=DoctorsClinic(150,'4pm-7pm');
dc3.clinics=c4;
d4.clinics.append(dc3);
d3.clinics.append(dc3);

db_session.add(s1);
db_session.add(s2);
db_session.add(s3);

db_session.add(ds1);
db_session.add(ds2);
db_session.add(ds3);
db_session.add(ds4);
db_session.add(c1);
db_session.add(c2);
db_session.add(c3);
db_session.add(c4);
db_session.add(dc1);
db_session.add(dc2);
db_session.add(dc3);
db_session.add(d1);
db_session.add(d2);
db_session.add(d3);
db_session.add(d4);
db_session.commit();
