from flask import Flask, render_template, request, session, redirect, url_for
from database import init_db, db_session 
from models import *


import json

init_db()

app = Flask('app')
@app.route('/', methods=['GET', 'POST'])
def home():
    specialityList = Speciality.query.all()
    clinics = Clinic.query.all()
    specialities=[]
    print specialityList 		
    [specialities.append(row.name) for row in specialityList]
    print specialities	
    locations=[]
    [locations.append(row.area) for row in clinics] 
    return render_template('index.html',locality=json.dumps(list(set(locations))),speciality=json.dumps(list(set(specialities))))

@app.route('/search', methods=['GET', 'POST'])
def search():
    locality = request.form['locality']
    speciality = request.form['speciality']
    doctors = Doctor.query.all()
    searchResultList=[];	
    doctorsBySpeciality=DoctorsSpeciality.query.filter_by(specialityFK=speciality).all()
    for doctor in doctorsBySpeciality:
    	clinics=DoctorsClinic.query.filter_by(doctorIdFK=doctor.doctorIdFK).all()
    	for clinic in clinics:
     		clinicInfo=Clinic.query.get(clinic.clinicIdFk)
        	if clinicInfo.area==locality:
       	        	doctorInfo=Doctor.query.get(doctor.doctorIdFK);
	        	searchResultList.append((doctorInfo,clinicInfo,clinic))
    if len(searchResultList) == 0:
        return "No Result Found"
    else:
        return render_template('search.html', docList = searchResultList)

