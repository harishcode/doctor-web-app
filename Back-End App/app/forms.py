from wtforms import Form, StringField, IntegerField, validators

class doctor_form(Form):
    id = IntegerField('id')
    name = StringField('Name', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Email(message='Enter a valid email id')])
    experience = IntegerField('Years of Experience')
    speciality = StringField('Speciality')
    education = StringField('Education')

class clinic_form(Form):
	id = IntegerField('id')
	name = StringField('Name')
	area = StringField('Area')
	doctor_id = IntegerField('doctor id')
	address = StringField('Address')
	city_name = StringField('City')
	fees = IntegerField('Fees')
	timings = StringField('Timing')
	contact_no = StringField('Contact Number')

class SpecialityForm(Form):
    sid = IntegerField('sid')
    name = StringField('Name', [validators.Length(min=4, max=25)])

