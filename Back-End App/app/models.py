from sqlalchemy import Column, Integer, String, ForeignKey
from database import Base
from sqlalchemy.orm import relationship, backref 


class Speciality(Base):
  __tablename__= 'tb_speciality'
  name=Column(String(100),primary_key=True)
  doctors=relationship('Doctor',secondary='tb_doctorsspeciality');
  def __init__(self,name):
    self.name=name  

class Doctor(Base):
  __tablename__ = 'tb_doctor'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String(100), nullable = False)
  email = Column(String(100), nullable = False, unique=True)
  experience = Column(Integer, nullable = False)
  education = Column(String(100), nullable = False)
  clinics=relationship('Clinic',secondary='tb_doctorsclinic');
  speciality = relationship('Speciality',secondary="tb_doctorsspeciality");
  
  def __init__(self, name, email, experience,education):
    self.name = name
    self.email = email
    self.experience = experience
    self.education = education

  def __repr__(self):
    return '<Doctor %r>' % (self.name)

class DoctorsSpeciality(Base):
  __tablename__='tb_doctorsspeciality'
  doctorIdFK=Column(Integer, ForeignKey('tb_doctor.id'),primary_key=True); 
  specialityFK=Column(String(100),ForeignKey('tb_speciality.name'),primary_key=True);
  doctorInfo=relationship('Doctor',backref=backref('doctorsRelated',cascade='save-update,merge,delete,all,delete-orphan'))
  specialityInfo=relationship('Speciality',backref=backref('specialitiesRelated',cascade='save-update,merge,delete,all,delete-orphan'))

class Clinic(Base):
  __tablename__ = 'tb_clinic'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String(50), nullable = False)
  area = Column(String(120), nullable = False)
  address = Column(String(300), nullable = False)
  city_name = Column(String(120), nullable = False)
  contact_no = Column(String(120), nullable = False, unique =True)
  doctors=relationship('Clinic',secondary='tb_doctorsclinic');

  def __init__(self,name, area, address, city_name, contact_no):
    self.name = name
    self.area = area
    self.address = address
    self.city_name = city_name
    self.contact_no = contact_no

  def __repr__(self):
    return '<Clinic %r>' % (self.name)

class DoctorsClinic(Base):
  __tablename__='tb_doctorsclinic'
  doctorIdFK=Column(Integer,ForeignKey('tb_doctor.id'),primary_key=True); 
  clinicIdFk=Column(Integer,ForeignKey('tb_clinic.id'),primary_key=True);
  fees = Column(Integer, nullable = False)
  timings = Column(String(120), nullable = False)
  relatedDoctors=relationship('Doctor',backref=backref('doctorsClinicAssoc',cascade='save-update,merge,delete,all,delete-orphan'));
  relatedClinics=relationship('Clinic',backref=backref('clinicDoctorAssoc',cascade='save-update,merge,delete,all,delete-orphan'));
  def __init__(self,fees,timings): 
    self.fees = fees
    self.timings = timings
