from flask import Flask, render_template, request, session, redirect, url_for
from database import init_db, db_session 
from models import *
from forms import doctor_form, clinic_form, SpecialityForm
from sqlalchemy import and_

init_db()

app = Flask('app')
specialities=[];

@app.route('/' , methods = ['GET', 'POST'])
def homepage():
    specialities=Speciality.query.all();    
    return render_template('/addDoctor.html',specialities=specialities);

@app.route('/addDoctor',methods =['GET','POST'])
def addDoctor():
    name=request.form['dName'];
    email=request.form['dEmail'];
    experience=request.form['dExperience'];
    education=request.form['degreeList'];
    doctor=Doctor(name,email,experience,education);
    specialityList=request.form['specialityList'].split(',');
    doctorspecialities=[];
    print specialityList;
    for specialityName in specialityList:
        speciality=Speciality.query.filter_by(name=specialityName).first();
        doctor.speciality.append(speciality);
    clinicCount=request.form['clinicCount'];
    print clinicCount;
    doctorclinics=[];
    clinicMap=dict();
    for i in range(1,int(clinicCount)):
        if 'clinicName'+str(i) in request.form:
            clinicName=request.form['clinicName'+str(i)];
            clinicArea=request.form['clinicArea'+str(i)];
            clinicAddress=request.form['clinicAddress'+str(i)];
            clinicLocation=request.form['clinicCity'+str(i)];
            clinicContact=request.form['clinicContact'+str(i)];
            clinicTimings=request.form['clinicTimings'+str(i)];
            clinicFees=request.form['clinicFees'+str(i)];  
            clinic=Clinic(clinicName,clinicArea,clinicAddress,clinicLocation,clinicContact);
            clinicMap[clinicName+clinicArea+clinicAddress+clinicLocation]=[clinicTimings,clinicFees];
            doctor.clinics.append(clinic);
            db_session.add(clinic);
    db_session.add(doctor);
    db_session.commit(); 
    for clinics in doctor.clinics:
        doctorsClinic=db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK==doctor.id,DoctorsClinic.clinicIdFk==clinics.id)).first();
        doctorsClinic.fees=clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][1];
        doctorsClinic.timings=clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][0];
        db_session.commit();
    print "ended";   
    return render_template('doctortest.html',did=doctor.id);    

@app.route('/editDoctor',methods =['GET','POST'])
def editDoctor():
    doctorId=request.form['doctorId']
    doctor=Doctor.query.get(doctorId)
    allSpecialities=Speciality.query.all()
    doctorSpecialities=doctor.speciality
    doctorClinics=doctor.clinics
    doctorClinicsInfo=[];
    for clinic in doctorClinics:
        doctorsClinic=db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK==doctor.id,DoctorsClinic.clinicIdFk==clinic.id)).first()
        doctorClinicsInfo.append(doctorsClinic);
    extraSpecialities=list(set(allSpecialities)-set(doctorSpecialities))
    doctorInfo=((doctor,doctorSpecialities,extraSpecialities,doctorClinics,doctorClinicsInfo));
    return render_template('editDoctor.html',doctorInfo=doctorInfo);
