from flask import Flask, render_template, request, session, redirect, url_for
from database import init_db, db_session 
from models import *
from forms import *
from sqlalchemy import and_
import json

init_db()

app = Flask('app')
specialities=[]
flag = 0
results_per_page=5
resultPerPage=[]
noOfPages=0

@app.route('/login', methods = ['GET', 'POST'])
@app.route('/login/', methods = ['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        flag = 1
        return redirect(url_for('adminLoginSuccess'))
    else:
        return render_template('login.html', form = form)

@app.route('/loginSuccess', methods = ['GET', 'POST'])
def adminLoginSuccess():
    if(not flag):
        return render_template('adminHome.html')

@app.route('/', methods=['GET', 'POST'])
def select():
    specialityList = Speciality.query.all()
    clinics = Clinic.query.all()
    specialities = []      
    [specialities.append(row.name) for row in specialityList] 
    locations = []
    [locations.append(row.area) for row in clinics] 
    return render_template('index.html',locality = json.dumps(list(set(locations))),speciality = json.dumps(list(set(specialities))))

@app.route('/add/' , methods = ['GET', 'POST'])
def homepage():
    specialities = Speciality.query.all()    
    return render_template('/addDoctor.html',specialities = specialities)

@app.route('/addDoctor',methods =[ 'GET','POST'])
def addDoctor():
    name = request.form['dName']
    email = request.form['dEmail']
    experience = request.form['dExperience']
    education = request.form['degreeList']
    doctor = Doctor(name,email,experience,education)
    specialityList = request.form['specialityList'].split(',')
    doctorspecialities = []
    for specialityName in specialityList:
        speciality = Speciality.query.filter_by(name = specialityName).first()
        doctor.speciality.append(speciality)
    clinicCount = request.form['clinicCount']
    doctorclinics = []
    clinicMap = dict()
    for i in range(1,int(clinicCount)):
        if 'clinicName'+str(i) in request.form:
            clinicName = request.form['clinicName'+str(i)]
            clinicArea = request.form['clinicArea'+str(i)]
            clinicAddress = request.form['clinicAddress'+str(i)]
            clinicLocation = request.form['clinicCity'+str(i)]
            clinicContact = request.form['clinicContact'+str(i)]
            clinicTimings = request.form['clinicTimings'+str(i)]
            clinicFees = request.form['clinicFees'+str(i)]  
            clinic = Clinic(clinicName,clinicArea,clinicAddress,clinicLocation,clinicContact)
            clinicMap[clinicName+clinicArea+clinicAddress+clinicLocation] = [clinicTimings,clinicFees]
            doctor.clinics.append(clinic)
            db_session.add(clinic)
    db_session.add(doctor)
    db_session.commit() 
    for clinics in doctor.clinics:
        doctorsClinic = db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK==doctor.id,DoctorsClinic.clinicIdFk==clinics.id)).first()
        doctorsClinic.fees = clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][1]
        doctorsClinic.timings = clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][0]
        db_session.commit()
    return render_template('doctorAdded.html',did = doctor.id)    

@app.route('/result/<int:pageNo>/<int:noOfPages>', methods=['GET', 'POST'])
def searchByPage(pageNo,noOfPages):
    return render_template('search.html', docList = resultPerPage[pageNo],flag=1,noOfPages=noOfPages)    

@app.route('/search', methods=['GET', 'POST'])
def search():
    locality = request.form['locality']
    speciality = request.form['speciality']
    searchResultList=[];    
    doctorsBySpeciality=DoctorsSpeciality.query.filter_by(specialityFK=speciality).all()
    for doctorSpeciality in doctorsBySpeciality:
        clinicDataFoundStatus=False
        doctor=doctorSpeciality.doctorInfo
        clinics=doctor.clinics;
        for clinic in clinics:
            if clinic.area==locality:
                clinicInfo=clinic.clinicDoctorAssoc;
                for doctorsClinic in clinicInfo:
                    if doctorsClinic.doctorIdFK==doctor.id:
                        clinicDataFoundStatus=True
                        searchResultList.append((doctor,clinic,doctorsClinic))
                if clinicDataFoundStatus==True:       
                    break
    resultLength=len(searchResultList); 
    if resultLength == 0:
        return render_template('noResults.html')
    else:
        noOfPages=resultLength/results_per_page;
        for page in range(0,noOfPages):
            startIndex=page*results_per_page;
            endIndex=(page+1)*results_per_page;
            resultPerPage.append(searchResultList[startIndex:endIndex]);
        if noOfPages==0:
            resultPerPage.append(searchResultList);         
        return render_template('search.html', docList = resultPerPage[0],flag=1,noOfPages=noOfPages)

@app.route('/EditDoctor/<int:doctorId>',methods = ['GET','POST'])
def editDoctor(doctorId):
    doctor = Doctor.query.get(doctorId)
    if doctor == None:    
        return render_template('doctorNotFound.html',did = doctorId)
    allSpecialities = Speciality.query.all()
    doctorSpecialities = doctor.speciality
    doctorClinics = doctor.clinics
    doctorClinicsInfo = []
    for clinic in doctorClinics:
        doctorsClinic = db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK == doctor.id,DoctorsClinic.clinicIdFk == clinic.id)).first()
        doctorClinicsInfo.append(doctorsClinic)
    extraSpecialities = list(set(allSpecialities) - set(doctorSpecialities))
    doctorInfo = ((doctor,doctorSpecialities,extraSpecialities,doctorClinics,doctorClinicsInfo))
    return render_template('editDoctor.html',doctorInfo = doctorInfo)

@app.route('/updateDoctor',methods = ['GET','POST'])
def updateDoctor():
    doctorId = request.form['doctorId']
    doctor = Doctor.query.get(doctorId)
    name = request.form['dName']
    email = request.form['dEmail']
    experience = request.form['dExperience']
    specialityList = request.form['specialityList'].split(',')
    for speciality in specialityList:
        doctorSpecialities = DoctorsSpeciality.query.filter(and_(DoctorsSpeciality.doctorIdFK == doctorId,DoctorsSpeciality.specialityFK == speciality)).first()
        if doctorSpecialities == None:
            doctorsSpeciality = DoctorsSpeciality()
            doctorsSpeciality.doctorIdFK = doctorId
            doctorsSpeciality.specialityFK = speciality
            db_session.add(doctorsSpeciality)
            db_session.commit()
    clinicCount = request.form['clinicCount']
    doctorclinics = []
    clinicMap = dict()
    for clinicNo in range(0,int(clinicCount)):
        if 'clinic'+str(clinicNo) in request.form:
            clinicId = request.form['clinic'+str(clinicNo)]
            clinicObject = Clinic.query.get(clinicId)
            clinicTimings = request.form['clinicTimings'+str(clinicNo)]
            clinicFees = request.form['clinicFees'+str(clinicNo)]  
            if clinicObject == None:
                clinicName = request.form['clinicName'+str(clinicNo)]
                clinicArea = request.form['clinicArea'+str(clinicNo)]
                clinicAddress = request.form['clinicAddress'+str(clinicNo)]
                clinicLocation = request.form['clinicLocation'+str(clinicNo)]
                clinicContact = request.form['clinicContact'+str(clinicNo)]
                clinic=Clinic(clinicName,clinicArea,clinicAddress,clinicLocation,clinicContact)
                clinicMap[clinicName+clinicArea+clinicAddress+clinicLocation] = [clinicTimings,clinicFees]
                doctor.clinics.append(clinic)
                db_session.add(clinic)
                db_session.commit() 
                for clinics in doctor.clinics:
                    doctorsClinic = db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK == doctor.id,DoctorsClinic.clinicIdFk==clinics.id)).first()
                    doctorsClinic.fees = clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][1]
                    doctorsClinic.timings = clinicMap[clinics.name+clinics.area+clinics.address+clinics.city_name][0]
                db_session.commit()
            else:       
                doctorsClinic = db_session.query(DoctorsClinic).filter(and_(DoctorsClinic.doctorIdFK == doctor.id,DoctorsClinic.clinicIdFk==clinicObject.id)).first()
                doctorsClinic.timings = clinicTimings
                doctorsClinic.fees = clinicFees
    doctor.name = name
    doctor.email = email
    doctor.experience = experience
    db_session.commit()
    return redirect(url_for('adminLoginSuccess'))

@app.route('/deleteDoctor/<int:doctorId>',methods = ['GET','POST'])
def deleteDoctor(doctorId):
    doctorObj = Doctor.query.get(doctorId)
    if (doctorObj != None):
        db_session.delete(doctorObj)
        db_session.commit()    
        return render_template('doctorDeleted.html',did = doctorId)
    else:
        return render_template('doctorDeleted.html',did = 0)

@app.route('/addSpeciality/<specName>')
def addSpeciality(specName):
    flag = 0
    speciality = Speciality.query.get(specName)
    if speciality == None:
        speciality=Speciality(specName)
        flag = 1
        db_session.add(speciality)
        db_session.commit()
        return render_template('SpecAdded.html',flag = flag)
    else:
        return render_template('SpecAdded.html',flag = flag)








