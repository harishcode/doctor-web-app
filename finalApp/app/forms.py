from wtforms import Form, StringField, IntegerField, validators, PasswordField

class LoginForm(Form):
    username = StringField('Username', [validators.Required(message='Required')])
    password = PasswordField('Password', [validators.Required(message='Required')])



